#!/bin/bash

echo "
██████╗ ███╗   ██╗███████╗██████╗ ██████╗ ██╗   ██╗████████╗███████╗
██╔══██╗████╗  ██║██╔════╝██╔══██╗██╔══██╗██║   ██║╚══██╔══╝██╔════╝
██║  ██║██╔██╗ ██║███████╗██████╔╝██████╔╝██║   ██║   ██║   █████╗  
██║  ██║██║╚██╗██║╚════██║██╔══██╗██╔══██╗██║   ██║   ██║   ██╔══╝  
██████╔╝██║ ╚████║███████║██████╔╝██║  ██║╚██████╔╝   ██║   ███████╗
╚═════╝ ╚═╝  ╚═══╝╚══════╝╚═════╝ ╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚══════╝
                Sub-Brute Created By Zero to Hero
"

if [ $# -eq 0 ]; then
  echo "No argument entered. Exiting."
  exit 1
fi


while getopts "d:" opt; do
  case $opt in
    d)
      domain="$OPTARG"
      ;;
    \?)
      echo "Please enter -d flag and enter your domain!"
      exit 1
      ;;
  esac
done


if [ ! -d '/tmp/dns_brute/'$domain ];
then
  mkdir /tmp/dns_brute/$domain
fi

echo '[+] Scanner began!'
echo ''

echo 'Subfinder is Running!'
echo ''
subfinder -d $domain -all -silent | anew /tmp/dns_brute/$domain/subdomain.txt
echo ''
echo '[+] Subfinder successful!'
echo ''

echo 'Merged Worldlist is Running!'
echo ''
cat /tmp/dns_brute/$domain/subdomain.txt | unfurl format %S | anew /tmp/dns_brute/best-dns-wordlist.txt
echo ''
echo '[+] Worldlist successfully created &  merged!'
echo ''

echo 'shuffledns is Running!'
echo ''
shuffledns -w /tmp/dns_brute/best-dns-wordlist.txt -d $domain -r /tmp/dns_brute/resolv.txt -silent -o /tmp/dns_brute/$domain/shuffledns.txt
echo ''
echo '[+] shuffledns successful!'
echo ''

echo 'DnsGen And Alterxis Running!'
echo ''
cat /tmp/dns_brute/$domain/shuffledns.txt /tmp/dns_brute/$domain/subdomain.txt | dnsgen - >> /tmp/dns_brute/$domain/dnsgen_result.txt
cat /tmp/dns_brute/$domain/shuffledns.txt /tmp/dns_brute/$domain/subdomain.txt | alterx -enrich >> /tmp/dns_brute/$domain/dnsgen_result.txt
echo ''
echo '[+] DnsGen And Alterx successful!'
echo ''

echo 'shuffledns is Running'
echo ''
shuffledns -w /tmp/dns_brute/$domain/dnsgen_result.txt -d $domain -r /tmp/dns_brute/resolv.txt -silent -o /tmp/dns_brute/$domain/shuffledns2.txt
echo ''
echo '[+] shuffledns successful!'
echo ''

echo 'Creating Target Dictionary for output!'

if [ ! -d "./targets" ];
then
  mkdir ./targets
fi

echo 'Target Dictionary created'
echo ''

echo 'Here is the final result :)'
echo ''
cat /tmp/dns_brute/$domain/shuffledns.txt /tmp/dns_brute/$domain/shuffledns2.txt | sort -u | anew ./targets/FULL_$domain.txt
echo ''
echo '[+] Sub-brute successful!'
echo ''
echo 'Have a good day :)'
