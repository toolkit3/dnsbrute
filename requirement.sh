#!/bin/bash

echo 'Check for dns_brute dir exist or not!'
echo ''

if [ ! -d "/tmp/dns_brute" ];
then
  mkdir /tmp/dns_brute
fi

echo '[+] dns_brute dir verified'
echo ''

echo 'Downloading Assetnote wordlist!'
echo ''

if [ ! -f "/tmp/dns_brute/best-dns-wordlist.txt" ];
then
wget -P /tmp/dns_brute/ https://wordlists-cdn.assetnote.io/data/manual/best-dns-wordlist.txt
fi

echo ''
echo '[+] Downloading the file is finished'
echo ''

echo 'Creating resolver file!'
echo ''
wget https://raw.githubusercontent.com/trickest/resolvers/main/resolvers-trusted.txt -O /tmp/dns_brute/resolv.txt
echo '[+] File created!'

echo 'Installing subfinder tool!'
echo ''
go install -v github.com/projectdiscovery/subfinder/v2/cmd/subfinder@latest
echo ''
echo '[+] subfinder Successfully installed'
echo ''

echo 'Installing unfurl tool!'
echo ''
go install github.com/tomnomnom/unfurl@latest
echo ''
echo '[+] unfurl Successfully installed'
echo ''

echo 'Installing pip3 if not installed!'
echo ''

if [ ! -f "/usr/bin/pip3" ];
then
        apt install python3-pip -y
fi

echo ''
echo '[+] pip3 Successfully installed'
echo ''

echo 'Installing dnsgen tool!'
echo ''
pip3 install dnsgen -q
echo ''
echo '[+] dnsgen Successfully installed'
echo ''

echo 'Installing massdns tool!'
echo ''

if [ ! -f "/usr/local/bin/massdns" ];
then
git clone https://github.com/blechschmidt/massdns.git
cd massdns
sudo make
make install
cd ../
rm -rf massdns
fi

echo ''
echo '[+] massdns Successfully installed'
echo ''

echo 'Installing shuffledns tool!'
echo ''
go install -v github.com/projectdiscovery/shuffledns/cmd/shuffledns@latest
echo ''
echo '[+] shuffledns Successfully installed'
echo ''

echo 'Installing anew tool!'
echo ''
go install -v github.com/tomnomnom/anew@latest
echo ''
echo '[+] anew Successfully installed'
