# dnsBrute
A tool for BruteForce DNS of a **Domain** that can help you to find nice **Subdomains** :).


## Installing:
```
git clone https://gitlab.com/toolkit3/dnsbrute.git
cd dnsbrute
chmod +x requirement.sh dnsBrute.sh
./requirement.sh
```

## Usage:
```
./dnsBrute.sh -d Domain

██████╗ ███╗   ██╗███████╗██████╗ ██████╗ ██╗   ██╗████████╗███████╗
██╔══██╗████╗  ██║██╔════╝██╔══██╗██╔══██╗██║   ██║╚══██╔══╝██╔════╝
██║  ██║██╔██╗ ██║███████╗██████╔╝██████╔╝██║   ██║   ██║   █████╗  
██║  ██║██║╚██╗██║╚════██║██╔══██╗██╔══██╗██║   ██║   ██║   ██╔══╝  
██████╔╝██║ ╚████║███████║██████╔╝██║  ██║╚██████╔╝   ██║   ███████╗
╚═════╝ ╚═╝  ╚═══╝╚══════╝╚═════╝ ╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚══════╝
                    Sub-Brute Created By Zero to Hero
```

##### Special Thanks to Zero To Hero Boys :)
